{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Setup
  ( stackStage
  , Options(..)
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Data.List qualified as List
import Geomancy (vec2, pattern WithVec2)
import RIO.Directory (doesDirectoryExist, getDirectoryContents)
import RIO.FilePath ((</>))
import RIO.State (gets, modify')
import RIO.Vector qualified as Vector
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Camera.Controls qualified as CameraControls
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Global.Resource.CubeMap.Base qualified as BaseCubeMap
import Global.Resource.Texture.Base qualified as BaseTexture
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun qualified as Sun
import Render.ImGui qualified as ImGui
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer qualified  as CommandBuffer
import Resource.Image qualified as Image
import Resource.Region qualified as Region
import Resource.Texture.Ktx2 qualified as Ktx2

import Global.Resource.Object qualified as Object
import Global.Resource.Static qualified as Static
import Stage.Main.Event.Drag qualified as Drag
import Stage.Main.Events qualified as Events
import Stage.Main.Render qualified as Render
import Stage.Main.Types (FrameResources(..), RunState(..), Stage)
import Stage.Main.UI qualified as UI
import Stage.Main.World.Cursor3d qualified as Cursor3d
import Stage.Main.World.DebugLines qualified as DebugLines
import Stage.Main.World.GridLines qualified as GridLines
import Stage.Main.World.Object.Selected qualified as ObjectSelected
import Stage.Main.World.Scene qualified as Scene
import Stage.Main.World.Tiles qualified as Tiles

data Options = Options
  { resourcePath :: FilePath
  }
  deriving (Show, Generic)

stackStage :: Options -> StackStage
stackStage = StackStage . stage

stage :: Options -> Stage
stage options = Engine.Stage
  { sTitle = "Main"

  , sAllocateRP = allocateRP
  , sAllocateP  = allocatePipelines
  , sInitialRS  = initialRunState options
  , sInitialRR  = initialFrameResources
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop = afterLoop
  }
  where
    allocateRP =
      Basic.allocate Basic.Settings
        { sShadowSize   = 2048
        , sShadowLayers = 1
        }

    allocatePipelines swapchain rps = do
      void $! ImGui.allocate swapchain (Basic.rpForwardMsaa rps) 0
      Basic.allocatePipelines_ swapchain rps

    beforeLoop = do
      (key, sink) <- Events.spawn
      modify' \ rs -> rs
        { rsEvents = Just sink
        }

      ImGui.beforeLoop True

      pure key

    afterLoop key = do
      ImGui.afterLoop
      Resource.release key

initialRunState :: Options -> StageRIO env (Resource.ReleaseKey, RunState)
initialRunState Options{..} = CommandBuffer.withPools \pools -> Region.run do
  perspective <- Camera.spawnPerspective
  ortho <- Camera.spawnOrthoPixelsCentered

  screen <- Engine.askScreenVar
  rsCursorPos <- Worker.newVar 0
  rsCursorP <-
    Worker.spawnMerge2
      (\Vk.Extent2D{width, height} (WithVec2 windowX windowY) ->
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      screen
      rsCursorPos

  rsDragState <- Drag.newState

  rsViewP <- CameraControls.spawnViewOrbital Camera.initialOrbitalInput

  rsCameraControls <- CameraControls.spawnControls rsViewP

  rsSceneV <- Worker.newVar Scene.initialInput
  rsSceneP <- Worker.spawnMerge3 Scene.mkScene perspective rsViewP rsSceneV
  rsSceneUiP <- Worker.spawnMerge1 Scene.mkSceneUi ortho

  rsUnitCube <- Region.local $
    Static.cubeWireframe pools

  rsUpdateShadow <- Worker.newVar ()

  resourceDirs <- getDirectoryContents resourcePath
  modelPacks <- fmap catMaybes $ for resourceDirs \name -> do
    let fullPath = resourcePath </> name
    isDir <- doesDirectoryExist fullPath
    if not isDir || ("." `List.isPrefixOf` name) then
      pure Nothing
    else
      pure $ Just (name, fullPath)
  let (packNames, packPaths) = List.unzip modelPacks

  loadedModels <- traverse (Object.loadModels pools) packPaths
  let (modelFiles, models) = List.unzip loadedModels

  let
    rsObjectPacks = Vector.fromList packNames
    rsObjectFiles = Vector.fromList modelFiles
    rsObjectCollections = Vector.fromList models

  -- (screenKey, rsScreenBoxP) <- Worker.registered Layout.trackScreen
  let assets = ()
  rsUI <- Region.local $ UI.spawn assets rsViewP

  rsSelectedObject <-
    ObjectSelected.spawn
      rsObjectCollections
      (UI.selectedObject rsUI)
      (UI.selectedOriginX rsUI)
      (UI.selectedOriginY rsUI)
      (UI.selectedOriginZ rsUI)

  rsGridLines <- traverse GridLines.spawn $
    Vector.fromList
      [
      ]
    -- XXX: useful for sub-tile positioning and aligning, when attached to cursor
    -- [ GridLines.Input
    --     { rows = 4
    --     , cols = 4
    --     , color = vec4 1 0.5 0 0.5
    --     , transform =
    --         Transform.rotateX (τ/4)
    --     }
    -- , GridLines.Input
    --     { rows = 4
    --     , cols = 4
    --     , color = vec4 0.25 1 0.25 0.5
    --     , transform =
    --         Transform.rotateY (τ/4)
    --     }
    -- , GridLines.Input
    --     { rows = 4
    --     , cols = 4
    --     , color = vec4 0 0.5 1 0.5
    --     , transform =
    --         Transform.rotateZ (τ/4)
    --     }
    -- ]

  rsTiles <- for rsObjectCollections \collection ->
    Tiles.spawnGroup $
      Vector.replicate (Vector.length collection)
        Tiles.Input
          { groupTiles = mempty
          , tileHeight = 0.5
          }

  rsDebugLines <- DebugLines.spawn []
      -- [ DebugLines.Edge
      --     { edgeFrom = DebugLines.Point 0 (vec4 1 0 0 1)
      --     , edgeTo   = DebugLines.Point (vec3 1 0 0) (vec4 1 0 0 1)
      --     }
      -- ]

  rsCursor3d <- Cursor3d.spawn screen rsSceneP rsCursorP (UI.currentLevel rsUI)

  rsCursorGrid <- Cursor3d.spawnGrid rsCursor3d

  rsTextures <- traverse (Ktx2.load pools) BaseTexture.sources
  rsCubeMaps <- traverse (Ktx2.load pools) BaseCubeMap.sources

  let rsEvents = Nothing
  pure RunState{..}

initialFrameResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources _pools passes pipelines = do

  let (_sunBB, lights) = Scene.staticLights

  lightsData <- Region.local $
    Buffer.allocateCoherent
      (Just "lightsData")
      Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      0
      lights

  textures <- gets rsTextures
  cubemaps <- gets rsCubeMaps
  frScene <- Set0.allocate
    (Basic.getSceneLayout pipelines)
    textures
    cubemaps
    (Just lightsData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Basic.rpShadowPass passes
    ]
    Nothing -- TODO: collect materials

  frSceneUi <- Set0.allocateEmpty
    (Basic.getSceneLayout pipelines)

  (frSunDescs, frSunData) <- Sun.createSet0Ds (Basic.getSunLayout pipelines)
  Buffer.updateCoherent lights frSunData

  frSelectedObject <- ObjectSelected.newObserver

  gridlines <- gets rsGridLines
  frGridLines <- for gridlines \_process ->
    GridLines.newObserver

  frTiles <- gets rsTiles >>= traverse (Tiles.newObserver 128)

  frDebugLines <- DebugLines.newObserver 100

  frCursor3d <- Cursor3d.newObserver
  frCursorGrid <- GridLines.newObserver

  frUpdateShadow <- Worker.newObserverIO ()

  pure FrameResources{..}
