module Stage.Main.Event.Types
  ( Event(..)
  , TileCoords
  , TileModelIx
  ) where

import RIO

import Geomancy (Vec2, Vec3)

import Stage.Main.World.Tiles qualified as Tiles
import Engine.Camera.Event.Type qualified as Camera

data Event
  = DoNothing
  | Camera Camera.Event
  | CameraPan Vec2
  | CameraZoom Float
  | PutLine Vec3 Vec3
  | WipeLines
  | TilePlace TileCoords TileModelIx Tiles.InputCell
  | TileRemove TileCoords TileModelIx
  | ChangeLevel Int
  | SceneStore FilePath
  | SceneLoad FilePath
  deriving (Show)

type TileCoords = (Int, Int, Int)

type TileModelIx = (Int, Int)
