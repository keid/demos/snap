module Stage.Main.Event.Drag where

import RIO

import Data.Type.Equality (type (~))
import Geomancy (Vec2, vec2)
import GHC.Float (double2Float)
import UnliftIO.Resource (MonadResource, ReleaseKey)

import Engine.Events.Sink (MonadSink, Sink(..))
import Engine.Window.CursorPos qualified as CursorPos
import Engine.Worker qualified as Worker

type DragState e = TMVar (Vec2, Vec2 -> Vec2 -> e)

newState :: MonadIO m => m (DragState e)
newState = newEmptyTMVarIO

callback
  :: ( MonadSink rs m
     , Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => DragState e
  -> cursor
  -------------------------
  -> Sink e rs
  -> m ReleaseKey
callback dragState cursorVar =
  CursorPos.callback . handler dragState cursorVar

handler
  :: ( MonadSink rs m
     , Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => DragState e
  -> cursor
  -> Sink e rs
  -> CursorPos.Callback m
handler dragState cursorVar (Sink signal) windowX windowY = do
  -- logDebug $ "CursorPos event: " <> displayShow cursorPos
  dragIO <- atomically do
    Worker.pushInputSTM cursorVar \_old ->
      cursorPos
    tryTakeTMVar dragState >>= \case
      Nothing ->
        pure Nothing
      Just (dragStart, dragHandler) -> do
        putTMVar dragState (cursorPos, dragHandler)
        pure $ Just do
          logDebug $ "Drag.handler: " <> displayShow (dragStart, cursorPos)
          signal $ dragHandler dragStart cursorPos
  sequence_ dragIO
  where
    cursorPos = vec2 (double2Float windowX) (double2Float windowY)

start
  :: ( MonadResource m
     , Worker.HasInput cursor
     , Worker.GetInput cursor ~ Vec2
     )
  => DragState e
  -> cursor
  -> (Vec2 -> Vec2 -> e)
  -> m Bool
start dragState cursor dragHandler = do
  -- logDebug $ "Drag.start: " <> displayShow cursorPos
  atomically do
    cursorPos <- Worker.getInputDataSTM cursor
    tryPutTMVar dragState (cursorPos, dragHandler)

stop
  :: MonadIO m
  => DragState e
  -> cursor
  -> m ()
stop dragState _cursor =
  atomically $
    tryTakeTMVar dragState >>= \case
      _ ->
        pure () -- XXX: consider stop handler
