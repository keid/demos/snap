module Stage.Main.Events
  ( spawn
  ) where

import RIO.Local

import Geomancy (vec3, vec4, pattern WithVec2)
import Geomancy.Transform qualified as Transform
import RIO.Map qualified as Map
import RIO.Partial (succ)
import RIO.State (MonadState, gets)
import RIO.Vector ((!?))
import System.Random (randomRIO)
import UnliftIO.Resource (ReleaseKey)

import Engine.Camera.Controls qualified as Camera
import Engine.Camera.Event.Handler qualified as CameraHandler
import Engine.Camera.Event.Type qualified as Camera
import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Events.Sink (MonadSink)
-- import Engine.Events.CursorPos qualified as CursorPos -- XXX: forked into Drag
import Engine.Window.Scroll qualified as Scroll
import Engine.Events.MouseButton qualified as MouseButton
import Engine.Window.Key (Key(..), KeyState(..))
import Engine.Window.Key qualified as Key
import Engine.Window.MouseButton (ModifierKeys(..), MouseButton(..), MouseButtonState(..))
import Engine.Worker qualified as Worker
import Render.ImGui qualified as ImGui

import Global.Resource.Tiles.Codec qualified as TilesCodec
import Stage.Main.Event.Types (Event(..))
import Stage.Main.Event.Drag qualified as Drag
import Stage.Main.Types (RunState(..))
import Stage.Main.UI qualified as UI
import Stage.Main.World.DebugLines qualified as DebugLines
import Stage.Main.World.GridLines qualified as GridLines
import Stage.Main.World.Tiles qualified as Tiles

spawn :: MonadSink RunState m => m (ReleaseKey, Events.Sink Event RunState)
spawn = do
  cursorWindow <- gets rsCursorPos
  cursorCentered <- gets rsCursorP
  dragState <- gets rsDragState
  Events.spawn handleEvent
    [ Drag.callback dragState cursorWindow
    , MouseButton.callback cursorCentered clickHandler
    , Scroll.callback . scrollHandler
    , Key.callback . keyHandler
    ]

handleEvent :: MonadSink RunState m => Event -> m ()
handleEvent = \case
  DoNothing ->
    pure ()

  WipeLines -> do
    debugLines <- gets rsDebugLines
    Worker.pushInput debugLines $ const mempty

  PutLine origin target -> do
    debugLines <- gets rsDebugLines
    col <- vec4
      <$> randomRIO (0, 1)
      <*> randomRIO (0, 1)
      <*> randomRIO (0, 1)
      <*> pure 0.8
    Worker.pushInput debugLines \old ->
      DebugLines.Edge
        { edgeFrom = DebugLines.Point origin col
        , edgeTo   = DebugLines.Point target col
        }
      : old

  TilePlace pos (setIx, modelIx) cell -> do
    modelSets <- gets rsTiles
    case modelSets !? setIx of
      Nothing ->
        pure ()
      Just tileGroups -> do
        for_ (zip [0..] $ toList tileGroups) \(workerIx, worker) ->
          when (workerIx /= modelIx) $ atomically do
            Tiles.Input{groupTiles} <- fmap Worker.vData . readTVar $ Worker.getInput worker
            when (Map.member pos groupTiles) $
              Worker.pushInputSTM worker \input -> input
                { Tiles.groupTiles =
                    Map.delete pos groupTiles
                }

        case tileGroups !? modelIx of
          Nothing ->
            pure ()
          Just worker ->
            Worker.pushInput worker \input@Tiles.Input{groupTiles} -> input
              { Tiles.groupTiles =
                  Map.alter
                    \case
                      Nothing ->
                        Just cell
                      Just tile ->
                        Just tile
                          { Tiles.tileDirection =
                              if Tiles.tileDirection tile == maxBound then
                                minBound
                              else
                                succ (Tiles.tileDirection tile)
                          }
                    pos
                    groupTiles
              }
        invalidateShadowMap

  TileRemove pos (setIx, _modelIx) -> do
    modelSets <- gets rsTiles
    case modelSets !? setIx of
      Nothing ->
        pure ()
      Just tileGroups -> do
        for_ (toList tileGroups) \worker ->
          atomically do
            Tiles.Input{groupTiles} <- fmap Worker.vData . readTVar $ Worker.getInput worker
            when (Map.member pos groupTiles) $
              Worker.pushInputSTM worker \input -> input
                { Tiles.groupTiles =
                    Map.delete pos groupTiles
                }
        invalidateShadowMap

  ChangeLevel delta -> do
    currentLevel <- gets (UI.currentLevel . rsUI)
    Worker.pushInput currentLevel \old ->
      old + delta

    cameraView <- gets rsViewP
    Worker.pushInput cameraView \voi -> voi
      { Camera.orbitTarget = Camera.orbitTarget voi + vec3 0 (fromIntegral delta * 0.5) 0
      }

    grids <- gets rsGridLines
    case grids !? 0 of
      Nothing ->
        pure ()
      Just grid ->
        Worker.pushInput grid \gi -> gi
          { GridLines.transform =
              GridLines.transform gi <>
              Transform.translate 0 (fromIntegral delta * 0.5) 0
          }

  Camera event ->
    CameraHandler.handler
      (gets rsViewP)
      (gets rsCameraControls)
      event

  CameraPan (WithVec2 x z) -> do
    vp <- gets rsViewP
    Camera.panInstant vp (vec3 x 0 $ -z)

  CameraZoom delta -> do
    cameraView <- gets rsViewP
    Worker.pushInput cameraView \voi -> voi
      { Camera.orbitDistance =
          max 0.5 $ Camera.orbitDistance voi - delta * 0.5
      }

  SceneStore file -> do
    tiles <- gets rsTiles
    packs <- gets rsObjectPacks
    files <- gets rsObjectFiles
    TilesCodec.store file tiles packs files

  SceneLoad file -> do
    tiles <- gets rsTiles
    packs <- gets rsObjectPacks
    files <- gets rsObjectFiles
    TilesCodec.load file tiles packs files
    invalidateShadowMap

invalidateShadowMap
  :: ( MonadIO m
     , MonadState RunState m
     )
  => m ()
invalidateShadowMap = do
  shadowVar <- gets rsUpdateShadow
  Worker.pushInput shadowVar id

clickHandler :: MonadSink RunState m => MouseButton.ClickHandler Event RunState m
clickHandler (Events.Sink signal) _cursorPos mouseEvent = ImGui.capturingMouse do
  case mouseEvent of

    (_mods, MouseButtonState'Pressed, MouseButton'3) -> do
      dragState <- gets rsDragState
      cursorWindow <- gets rsCursorPos
      _started <- Drag.start dragState cursorWindow \dragStart dragPos ->
        CameraPan $ dragPos - dragStart
      pure ()

    (_mods, MouseButtonState'Released, MouseButton'3) -> do
      dragState <- gets rsDragState
      cursorWindow <- gets rsCursorPos
      Drag.stop dragState cursorWindow

    (_mods, MouseButtonState'Released, _btn) ->
      -- XXX: ignore, unless dragging
      pure ()

    (ModifierKeys{..}, _state, MouseButton'1) -> do
      cursor3d <- gets rsCursor3d
      Worker.getOutputData cursor3d >>= \case
        (Nothing, _buf) ->
          pure ()
        (Just (we, ns, level), _buf) -> do
          tileIx <- gets (UI.selectedObject . rsUI) >>=
            Worker.getInputData
          if modifierKeysShift then
            signal $ TileRemove (we, ns, level) tileIx
          else do
            selectedObject <- gets rsSelectedObject
            (origin, _transform, _mobject) <- Worker.getOutputData selectedObject
            signal $ TilePlace (we, ns, level) tileIx Tiles.InputCell
              { tileScale     = 1.0
              , tileDirection = Tiles.North
              , tileOrigin    = origin
              }

    (_mods, _state, MouseButton'4) ->
      signal $ ChangeLevel 1

    (_mods, _state, MouseButton'5) ->
      signal $ ChangeLevel (-1)

    (_mods, _state, btn) ->
      logDebug $ "Mouse button: " <> displayShow btn

scrollHandler :: MonadSink RunState m => Events.Sink Event RunState -> Double -> Double -> m ()
scrollHandler (Events.Sink signal) dx dy = ImGui.capturingMouse do
  logDebug $ "Scroll: " <> displayShow (dx, dy)
  when (dy /= 0) do
    signal $ CameraZoom (double2Float dy)

keyHandler :: MonadSink RunState m => Events.Sink Event RunState -> Key.Callback m
keyHandler (Events.Sink signal) _keyCode keyEvent = ImGui.capturingKeyboard do
  let (_mods, keyState, key) = keyEvent
  -- logDebug $ "Key event (" <> display keyCode <> "): " <> displayShow keyEvent

  case (keyState, key) of
    (KeyState'Pressed, Key'Right) ->
      signal . Camera $ Camera.PanHorizontal (-1)
    (KeyState'Released, Key'Right) ->
      signal . Camera $ Camera.PanHorizontal 0

    (KeyState'Pressed, Key'Left) ->
      signal . Camera $ Camera.PanHorizontal 1
    (KeyState'Released, Key'Left) ->
      signal . Camera $ Camera.PanHorizontal 0

    (KeyState'Pressed, Key'Up) ->
      signal . Camera $ Camera.PanVertical (-1)
    (KeyState'Released, Key'Up) ->
      signal . Camera $ Camera.PanVertical 0

    (KeyState'Pressed, Key'Down) ->
      signal . Camera $ Camera.PanVertical 1
    (KeyState'Released, Key'Down) ->
      signal . Camera $ Camera.PanVertical 0

    (KeyState'Pressed, Key'Insert) ->
      signal . Camera $ Camera.TurnAzimuth (-1)
    (KeyState'Released, Key'Insert) ->
      signal . Camera $ Camera.TurnAzimuth 0

    (KeyState'Pressed, Key'Delete) ->
      signal . Camera $ Camera.TurnAzimuth 1
    (KeyState'Released, Key'Delete) ->
      signal . Camera $ Camera.TurnAzimuth 0

    (KeyState'Pressed, Key'Home) ->
      signal . Camera $ Camera.TurnInclination 1
    (KeyState'Released, Key'Home) ->
      signal . Camera $ Camera.TurnInclination 0

    (KeyState'Pressed, Key'End) ->
      signal . Camera $ Camera.TurnInclination (-1)
    (KeyState'Released, Key'End) ->
      signal . Camera $ Camera.TurnInclination 0

    (KeyState'Pressed, Key'PageUp) ->
      signal $ ChangeLevel (-1)
    (KeyState'Pressed, Key'PageDown) ->
      signal $ ChangeLevel 1

    _ ->
      pure ()
