module Stage.Main.Render.UI
  ( imguiDrawData
  ) where

import RIO.Local

import DearImGui qualified
import RIO.Map qualified as Map
import RIO.Text qualified as Text
import RIO.Vector ((!?))
import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Events.Sink (Sink(..))
import Engine.Types (StageFrameRIO, askScreenVar)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.ImGui qualified as ImGui
import Resource.Gltf.Weld qualified as Weld
import Resource.Gltf.Weld.Settings qualified as Settings
import Resource.Mesh.Types qualified as Mesh

import Global.Resource.Object qualified as Object
import Stage.Main.Event.Types qualified as Event
import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.UI qualified as UI

type DrawM = StageFrameRIO Basic.RenderPasses Basic.Pipelines FrameResources RunState

imguiDrawData :: DrawM DearImGui.DrawData
imguiDrawData = do
  ui <- stageFrameGetRS rsUI

  Vk.Extent2D screenWidth screenHeight <-
    mapRIO fst askScreenVar >>= Worker.getInputData

  fmap snd $ ImGui.mkDrawData do
    DearImGui.withWindowOpen "Scene" do
      sceneView <- stageFrameGetRS rsViewP

      let
        angleRef = Worker.stateVarMap
          Camera.orbitAzimuth
          (\new voi -> voi
              { Camera.orbitAzimuth = new
              }
          )
          (Worker.getInput sceneView)
      void $! DearImGui.sliderAngle "Azimuth" angleRef (-360) 360

      let
        ascentRef = Worker.stateVarMap
          Camera.orbitAscent
          (\new voi -> voi
              { Camera.orbitAscent = new
              }
          )
          (Worker.getInput sceneView)
      void $! DearImGui.sliderAngle "Ascent" ascentRef (-89) 89

      let
        distanceRef = Worker.stateVarMap
          Camera.orbitDistance
          (\new voi -> voi
              { Camera.orbitDistance = new
              }
          )
          (Worker.getInput sceneView)
      void $! DearImGui.sliderFloat "Distance" distanceRef (1/512) 10.0

    DearImGui.setNextWindowPos
      (pure @IO $ DearImGui.ImVec2 200 $ fromIntegral screenHeight - 200)
      DearImGui.ImGuiCond_Always
      Nothing
    DearImGui.setNextWindowSize
      (pure @IO $ DearImGui.ImVec2 (fromIntegral screenWidth - 201) 200)
      DearImGui.ImGuiCond_Always
    DearImGui.withWindowOpen "Terrain" do
      tabOpen <- newIORef True
      DearImGui.withTabBarOpen "##stuff" DearImGui.ImGuiTabBarFlags_NoCloseWithMiddleMouseButton do
        packs <- stageFrameGetRS rsObjectPacks
        files <- stageFrameGetRS rsObjectFiles

        let ixs = Vector.imap const packs

        for_ (Vector.zip3 ixs packs files) \(ix, pack, file) ->
          DearImGui.withTabItemOpen (fromString pack) tabOpen DearImGui.ImGuiTabItemFlags_NoCloseWithMiddleMouseButton do
            selectedObject <- stageFrameGetRS rsSelectedObject
            Worker.Versioned{vVersion=oldVer} <- readTVarIO (Worker.getOutput selectedObject)
            changed <- DearImGui.listBox
              (fromString pack <> "##list")
              (Worker.stateVarMap
                snd
                (\new _old -> (ix, new))
                (UI.selectedObject ui)
              )
              (map fromString $ Map.keys file)
            when changed do
              void $! async $ atomically do
                Worker.Versioned{vVersion=newVer, vData=selected} <- readTVar (Worker.getOutput selectedObject)
                unless (newVer > oldVer) retrySTM
                let (_originV, _attrs, mobject) = selected
                case mobject of
                  Nothing ->
                    pure ()
                  Just object -> do
                    let settings = Object.settings object
                    Worker.pushInputSTM (UI.selectedOriginX ui) $ const (Just $ Settings.originX settings)
                    Worker.pushInputSTM (UI.selectedOriginY ui) $ const (Just $ Settings.originY settings)
                    Worker.pushInputSTM (UI.selectedOriginZ ui) $ const (Just $ Settings.originZ settings)

    DearImGui.withWindowOpen "Object" do
      collections <- stageFrameGetRS rsObjectCollections
      (collectionIx, modelIx) <- Worker.getInputData $ UI.selectedObject ui
      (originV, _attrs, _mobject) <- stageFrameGetRS rsSelectedObject >>= Worker.getOutputData
      case collections !? collectionIx of
        Nothing ->
          pure ()
        Just models ->
          case models !? modelIx of
            Nothing ->
              pure ()
            Just object -> do
              let originX = UI.selectedOriginX ui
              let originY = UI.selectedOriginY ui
              let originZ = UI.selectedOriginZ ui

              DearImGui.text . fromString $ Object.source object
              DearImGui.newLine

              DearImGui.text "Origin"
              DearImGui.sameLine
              DearImGui.text $ showText originV
              let
                originButton var current label origin = do
                  DearImGui.sameLine
                  let mark = bool "" ">" (current == Just origin)
                  DearImGui.button (mark <> label) >>= \clicked ->
                    when clicked do
                      Worker.pushInput var \_current ->
                        if current == Just origin then
                          Nothing
                        else
                          Just origin

              DearImGui.text "X:"
              currentX <- Worker.getInputData originX
              originButton originX currentX "Left##ox"   Weld.OriginXLeft
              originButton originX currentX "Middle##ox" Weld.OriginXMiddle
              originButton originX currentX "Mean##ox"   Weld.OriginXMean
              originButton originX currentX "Right##ox"  Weld.OriginXRight

              DearImGui.text "Y:"
              currentY <- Worker.getInputData originY
              originButton originY currentY "Top##oy"    Weld.OriginYTop
              originButton originY currentY "Middle##oy" Weld.OriginYMiddle
              originButton originY currentY "Mean##oy"   Weld.OriginYMean
              originButton originY currentY "Bottom##oy" Weld.OriginYBottom

              DearImGui.text "Z:"
              currentZ <- Worker.getInputData originZ
              originButton originZ currentZ "Near##oz"   Weld.OriginZNear
              originButton originZ currentZ "Middle##oz" Weld.OriginZMiddle
              originButton originZ currentZ "Mean##oz"   Weld.OriginZMean
              originButton originZ currentZ "Far##oz"    Weld.OriginZFar

              DearImGui.button "Save as default" >>= \clicked ->
                when clicked do
                  mox <- Worker.getInputData originX
                  moy <- Worker.getInputData originY
                  moz <- Worker.getInputData originZ
                  let
                    settings = Object.settings object
                    -- (ox, oy, oz) = Settings.measuredOrigin settings
                    updated = settings
                      { Settings.originX = fromMaybe (Settings.originX settings) mox
                      , Settings.originY = fromMaybe (Settings.originY settings) moy
                      , Settings.originZ = fromMaybe (Settings.originZ settings) moz
                      }
                  Object.storeSettings (Object.settingsFile object) updated

              let Mesh.Meta{..} = Object.meta object

              treeNode "Indices" do
                treeNode "Scene indices" do
                  DearImGui.text $ showText mOpaqueIndices
                  DearImGui.text $ showText mBlendedIndices
                treeNode "Node indices" do
                  DearImGui.text $ showText mOpaqueNodes
                  DearImGui.text $ showText mBlendedNodes

              treeNode "Measures" do
                DearImGui.text "Bounding sphere:"
                DearImGui.text $ showText mBoundingSphere
                DearImGui.newLine
                DearImGui.text "Bounding box mat4:"
                DearImGui.text $ showText mTransformBB
                DearImGui.newLine

                let Mesh.AxisAligned{..} = mMeasurements
                DearImGui.text "AA-measures"

                DearImGui.bullet
                DearImGui.text . fromString $ "X:" <> dropWhile (/= ' ') (show aaX)

                DearImGui.bullet
                DearImGui.text . fromString $ "Y:" <> dropWhile (/= ' ') (show aaY)

                DearImGui.bullet
                DearImGui.text . fromString $ "Z:" <> dropWhile (/= ' ') (show aaZ)

    DearImGui.withWindowOpen "Scene files" do
      void $! DearImGui.inputText
        "Scene file (without extension)"
        ( Worker.stateVarMap
            Text.pack
            (\t _old -> Text.unpack t)
            (UI.sceneFile ui)
        )
        1024

      DearImGui.button "Save" >>= \clicked ->
        when clicked $
          stageFrameGetRS rsEvents >>= \case
            Nothing ->
              logError "RunState is not ready to receive events"
            Just (Sink signal) -> do
              sceneFile <- Worker.getOutputData $ UI.sceneFile ui
              mapRIO fst . signal $ Event.SceneStore sceneFile
      DearImGui.sameLine
      DearImGui.button "Load" >>= \clicked ->
        when clicked $
          stageFrameGetRS rsEvents >>= \case
            Nothing ->
              logError "RunState is not ready to receive events"
            Just (Sink signal) -> do
              sceneFile <- Worker.getOutputData $ UI.sceneFile ui
              mapRIO fst . signal $ Event.SceneLoad sceneFile

    -- DearImGui.showMetricsWindow
    -- DearImGui.showDemoWindow

treeNode :: MonadIO m => Text -> m t -> m ()
treeNode label contents =
  DearImGui.treeNode label >>= \open ->
    when open do
      contents
      DearImGui.separator
      DearImGui.treePop

showText :: Show a => a -> Text
showText = fromString . show
