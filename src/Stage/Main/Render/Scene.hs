{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Render.Scene
  ( prepareCasters
  , prepareScene
  ) where

import RIO.Local

import Geomancy (Transform)
import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk

import Engine.Types (StageFrameRIO)
import Engine.Vulkan.DescSets (Bound, Compatible)
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 (Scene)
import Render.Draw qualified as Draw
import Resource.Model (Vertex3d)

import Global.Resource.Object qualified as Object
import Stage.Main.Types (FrameResources(..), RunState(..))

type DrawM = StageFrameRIO Basic.RenderPasses Basic.Pipelines FrameResources RunState

type DrawCasters dsl m = Vk.CommandBuffer -> Bound dsl (Vertex3d ()) Transform m ()

type DrawData dsl m =
  ( Vk.CommandBuffer -> Bound dsl Void Void m ()
  , Vk.CommandBuffer -> Bound dsl Void Void m ()
  )

prepareCasters
  :: ( -- Compatible '[Sun] dsl
     )
  => FrameResources
  -> DrawM (DrawCasters dsl DrawM)
prepareCasters FrameResources{..} = do
  collections <- stageFrameGetRS rsObjectCollections

  tileSets <- traverse (traverse Worker.readObservedIO) frTiles

  let
    drawCasters cb =
      for_ (Vector.zip collections tileSets) \(collection, tiles) ->
        flip Vector.imapM_ tiles \tileIx tileInstances ->
          case collection Vector.!? tileIx of
            Nothing ->
              pure ()
            Just found ->
              Draw.indexedPos cb (Object.model found) tileInstances

  pure drawCasters

prepareScene
  :: ( Compatible '[Scene] dsl
     )
  => Basic.Pipelines
  -> FrameResources
  -> DrawM (DrawData dsl DrawM)
prepareScene Basic.Pipelines{..} FrameResources{..} = do
  collections <- stageFrameGetRS rsObjectCollections
  cubeModel <- stageFrameGetRS (fst . rsUnitCube)

  gridLines <- traverse Worker.readObservedIO frGridLines
  (debugLinesModel, debugLinesInstance) <- Worker.readObservedIO frDebugLines

  cursor3d <- Worker.readObservedIO frCursor3d
  (cursorGrid, cursorGridInstance) <- Worker.readObservedIO frCursorGrid

  tileSets <- traverse (traverse Worker.readObservedIO) frTiles

  -- selectedObject <- stageFrameGetRS rsSelectedObject
  -- (_origin, _transform, mobject) <- Worker.getOutputData selectedObject
  -- selectedObjectInstance <- Worker.readObservedIO frSelectedObject

  -- let (sunBox, _lights) = Scene.staticLights
  -- context <- asks fst
  -- (_, sunBoxInstance) <- Buffer.allocateCoherent context Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT 1 [sunBox]

  let
    drawOpaque cb = do
      Graphics.bind cb pLitColored do
        for_ (Vector.zip collections tileSets) \(collection, tiles) ->
          flip Vector.imapM_ tiles \tileIx tileInstances ->
            case collection Vector.!? tileIx of
              Nothing ->
                pure ()
              Just found ->
                Draw.indexed cb (Object.model found) tileInstances

        -- for_ mobject \selected ->
        --   Draw.indexed cb (Object.model selected) selectedObjectInstance -- cursor3d

      Graphics.bind cb pWireframe do
        Draw.indexed cb cubeModel cursor3d
        Draw.indexed cb cursorGrid cursorGridInstance
        Draw.indexed cb debugLinesModel debugLinesInstance
        traverse_ (uncurry $ Draw.indexed cb) gridLines
        -- Draw.indexed cb cubeModel sunBoxInstance

    drawBlended _cb = do
      pure ()

  pure (drawOpaque, drawBlended)
