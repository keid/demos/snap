{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO.Local

import Vulkan.Core10 qualified as Vk

import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui
import Render.ShadowMap.RenderPass qualified as ShadowPass

import Stage.Main.Render.Scene (prepareCasters, prepareScene)
import Stage.Main.Render.UI (imguiDrawData)
import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.World.Cursor3d qualified as Cursor3d
import Stage.Main.World.DebugLines qualified as DebugLines
import Stage.Main.World.GridLines qualified as GridLines
import Stage.Main.World.Object.Selected qualified as ObjectSelected
import Stage.Main.World.Tiles qualified as Tiles

updateBuffers
  :: RunState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene
  Set0.observe rsSceneUiP frSceneUi

  ObjectSelected.observe rsSelectedObject frSelectedObject
  traverse_ (uncurry GridLines.observe) $ zip (toList rsGridLines) (toList frGridLines)
  DebugLines.observe rsDebugLines frDebugLines

  Cursor3d.observe rsCursor3d frCursor3d
  GridLines.observe rsCursorGrid frCursorGrid

  traverse_ (uncurry Tiles.observe) $ zip (toList rsTiles) (toList frTiles)

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb fr@FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  sceneDrawCasters <- prepareCasters fr
  (sceneDrawOpaque, sceneDrawBlended) <- prepareScene fPipelines fr
  dear <- imguiDrawData

  updateShadow <- stageFrameGetRS rsUpdateShadow
  Worker.observeIO_ updateShadow frUpdateShadow \() () -> do
    Worker.Versioned{vVersion} <- readTVarIO (Worker.getInput updateShadow)
    logDebug $ "Updating shadowmap for version " <> displayShow vVersion
    let shadowLayout = Pipeline.pLayout (Basic.pShadowCast fPipelines)
    ShadowPass.usePass (Basic.rpShadowPass fRenderpass) imageIndex cb do
      let shadowArea = ShadowPass.smRenderArea (Basic.rpShadowPass fRenderpass)
      Swapchain.setDynamic cb shadowArea shadowArea
      withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS shadowLayout frSunDescs $
        Graphics.bind cb (Basic.pShadowCast fPipelines) $
          sceneDrawCasters cb

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene (Basic.pWireframe fPipelines) cb do
      sceneDrawOpaque cb
      sceneDrawBlended cb

    ImGui.draw dear cb
