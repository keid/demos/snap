module Stage.Main.World.Cursor3d
  ( Process
  , spawn
  , mkCursor3d

  , spawnGrid
  , mkCursorGrid

  , Observer
  , newObserver
  , observe
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (MonadResource, ResourceT)
import Data.Type.Equality (type (~))
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Geomancy (Vec2, vec2, vec3, withVec2, withVec3)
import Geomancy.Transform qualified as Transform
import Geometry.Hit qualified as Hit
import Geometry.Intersect (ray'plane)
import Geometry.Plane qualified as Plane
import Geometry.Ray qualified as Ray
import Render.DescSets.Set0 (Scene(..))
import Render.Lit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

import Stage.Main.World.GridLines qualified as GridLines

type Process = Worker.Merge Attrs

type Attrs =
  ( Maybe (Int, Int, Int)
  , Storable.Vector UnlitColored.InstanceAttrs
  )
spawn
  :: ( MonadResource m
     , MonadUnliftIO m
     , Worker.HasOutput screen
     , Worker.GetOutput screen ~ Vk.Extent2D
     , Worker.HasOutput scene
     , Worker.GetOutput scene ~ Scene
     , Worker.HasOutput cursorPos
     , Worker.GetOutput cursorPos ~ Vec2
     , Worker.HasOutput currentLevel
     , Worker.GetOutput currentLevel ~ Int
     )
  => screen
  -> scene
  -> cursorPos
  -> currentLevel
  -> m Process
spawn = Worker.spawnMerge4 mkCursor3d

mkCursor3d
  :: Vk.Extent2D
  -> Scene
  -> Vec2
  -> Int
  -> Attrs
mkCursor3d screen scene cursorPos currentLevel =
  case ray'plane (Ray.fromSegment origin target) plane of
    Nothing ->
      ( Nothing
      , mempty
      )
    Just hit ->
      withVec3 (Hit.position hit) \x _y z ->
        let
          we = round x
          ns = round z
        in
          ( Just (we, ns, currentLevel)
          , Storable.singleton $
              Transform.translate
                (fromIntegral we)
                y
                (fromIntegral ns)
          )
  where
    tileHeight = 0.5
    y = fromIntegral currentLevel * tileHeight
    plane = Plane.xzUp (y + 0.5)

    Vk.Extent2D{width=iWidth, height=iHeight} = screen
    halfWidth = fromIntegral iWidth / 2
    halfHeight = fromIntegral iHeight / 2

    (curX, curY) = withVec2 (cursorPos / vec2 halfWidth halfHeight) (,)
    zNear = 1/2048
    zFar = 1 - 1/16384

    Scene{sceneInvProjection, sceneInvView} = scene
    inverse = sceneInvProjection <> sceneInvView
    unproject curZ = Transform.apply (vec3 curX curY curZ) inverse

    origin = unproject zNear
    target = unproject zFar

spawnGrid
  :: ( MonadResource m
     , MonadUnliftIO m
     )
  => Process
  -> m (Worker.Merge GridLines.Output)
spawnGrid = Worker.spawnMerge1 (mkCursorGrid . fst)

mkCursorGrid :: Maybe (Int, Int, Int) -> GridLines.Output
mkCursorGrid mcell = GridLines.mkGridLines input
  where
    units = 5

    input = case mcell of
      Nothing ->
        GridLines.emptyInput
      Just (ew, ns, level) ->
        GridLines.Input
          { rows  = units
          , cols  = units
          , color = 0.5
          , transform = mconcat
              [ Transform.scale (fromIntegral units)
              , Transform.rotateX (τ/4)
              , Transform.translate
                  (fromIntegral ew)
                  (fromIntegral level * 0.5 + 0.5 - 1/256)
                  (fromIntegral ns)
              ]
          }

type Buffer = Buffer.Allocated 'Buffer.Coherent UnlitColored.InstanceAttrs

type Observer = Worker.ObserverIO Buffer

newObserver :: ResourceT (Engine.StageRIO st) Observer
newObserver =
  Buffer.newObserverCoherent
    "Cursor3d.instances"
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    mempty

observe
  :: ( HasVulkan env
     , Worker.HasOutput source
     , Worker.GetOutput source ~ Attrs
     )
  => source
  -> Observer
  -> RIO env ()
observe process observer =
  Worker.observeIO_ process observer \buf (_mpos, attrs) ->
    Buffer.updateCoherentResize_ buf attrs
