module Stage.Main.World.Scene where

import RIO.Local

import Geomancy (Transform, Vec3, Vec4, vec3, vec4)
import Geomancy.Vec4 qualified as Vec4
import RIO.Vector.Storable qualified as Storable

import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Render.DescSets.Set0 (Scene(..), emptyScene)
import Render.DescSets.Sun (Sun(..), SunInput(..), initialSunInput, mkSun)

type InputVar = Worker.Var Input

data Input = Input

initialInput :: Input
initialInput = Input

type Process = Worker.Merge Scene

mkScene :: Camera.Projection 'Camera.Perspective -> Camera.View -> Input -> Scene
mkScene Camera.Projection{..} Camera.View{..} Input = Scene{..}
  where
    sceneProjection    = projectionTransform
    sceneInvProjection = projectionInverse

    sceneView          = viewTransform
    sceneInvView       = viewTransformInv
    sceneViewPos       = viewPosition
    sceneViewDir       = viewDirection

    sceneFog           = 0
    sceneEnvCube       = -1
    sceneNumLights     = fromIntegral $ Storable.length $ snd staticLights
    sceneTweaks        = 0

mkSceneUi :: Camera.Projection 'Camera.Orthographic -> Scene
mkSceneUi Camera.Projection{..} =
  emptyScene
    { sceneProjection    = projectionTransform
    , sceneInvProjection = projectionInverse
    }

staticLights :: (Transform, Storable.Vector Sun)
staticLights = (sunBB, lights)
  where
    (sunBB, theSun) = mkSun initialSunInput
      { siAzimuth     = τ/9
      , siInclination = τ/10
      , siColor       = 1.5
      , siSize        = 16
      , siShadowIx    = 0
      }

    lights =
      Storable.fromList
        [ theSun
        , noShadow
            (vec3 0 (-10) 0)
            (vec3 0 (-1) 0)
            0.125
        , noShadow
            (vec3 0 10 0)
            (vec3 0 1 0)
            0.125
        ]

noShadow :: Vec3 -> Vec3 -> Vec4 -> Sun
noShadow pos dir color = Sun
  { sunViewProjection = mempty
  , sunShadow         = vec4 0 0 (-1) 0
  , sunPosition       = Vec4.fromVec3 pos 0
  , sunDirection      = Vec4.fromVec3 dir 0
  , sunColor          = color
  }
