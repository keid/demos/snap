module Stage.Main.UI
  ( UI(..)
  , spawn
  ) where

import RIO hiding (display)

import Control.Monad.Trans.Resource qualified as Resource

import Engine.Camera qualified as Camera
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Resource.Gltf.Weld qualified as Weld

data UI = UI
  { display :: Worker.Var Bool

  , sceneFile :: Worker.Var FilePath
    -- TODO: "unsaved" marker?

  , currentLevel :: Worker.Var Int

  , selectedObject :: Worker.Var (Int, Int)

  , selectedOriginX :: Worker.Var (Maybe Weld.Origin)
  , selectedOriginY :: Worker.Var (Maybe Weld.Origin)
  , selectedOriginZ :: Worker.Var (Maybe Weld.Origin)
  }

type Assets = ()

spawn
  :: Assets
  -- -> Layout.BoxProcess
  -> Camera.ViewProcess
  -- -> Worker.Var sceneInput
  -- -> Env.Process
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
spawn _assets _viewP = do
  display <- Worker.newVar True

  sceneFile <- Worker.newVar "./scenes/file"

  currentLevel <- Worker.newVar 0

  selectedObject <- Worker.newVar (0, 0)

  selectedOriginX <- Worker.newVar Nothing
  selectedOriginY <- Worker.newVar Nothing
  selectedOriginZ <- Worker.newVar Nothing

  key <- Resource.register $ traverse_ Resource.release
    []

  pure (key, UI{..})
