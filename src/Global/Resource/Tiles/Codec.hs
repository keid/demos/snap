{-# OPTIONS_GHC -Wwarn=orphans #-}

module Global.Resource.Tiles.Codec
  ( store
  , load
  ) where

import RIO

import Codec.Compression.Zstd qualified as Zstd
import Codec.Serialise qualified as CBOR
import Geomancy (Vec3, withVec3)
import Geomancy.Vec3 qualified as Vec3
import RIO.ByteString qualified as ByteString
import RIO.ByteString.Lazy qualified as BSL
import RIO.FilePath ((</>), (<.>))
import RIO.Map qualified as Map
import RIO.Vector ((!?))
import RIO.Vector qualified as Vector

import Engine.Worker qualified as Worker
import Resource.Compressed.Zstd qualified as Zstd

import Stage.Main.World.Tiles qualified as Tiles

data PackageV1 = PackageV1
  { v1_objects :: Vector (FilePath, Vector (FilePath, Tiles.Input))
  }
  deriving (Show, Generic)

instance CBOR.Serialise PackageV1

instance CBOR.Serialise Tiles.Input
instance CBOR.Serialise Tiles.InputCell
instance CBOR.Serialise Tiles.Direction

instance CBOR.Serialise Vec3 where
  encode v = CBOR.encode $ withVec3 v (,,)
  decode = fmap Vec3.fromTuple CBOR.decode

-- TODO: use zstd

store
  :: ( MonadReader env m
     , HasLogFunc env
     , MonadUnliftIO m
     )
  => FilePath
  -> Vector Tiles.ProcessGroup
  -> Vector FilePath
  -> Vector (Map FilePath Int)
  -> m ()
store file groups packs files = do
  logInfo $ "Storing scene to " <> fromString file
  v0_tiles <- atomically do
    for groups \group ->
      for group \worker ->
        fmap Worker.vData . readTVar $ Worker.getInput worker
  -- liftIO $ writeFileSerialise file v0_tiles

  let
    v1 = PackageV1 do
      (pack, packMap, tiles) <- Vector.zip3 packs files v0_tiles
      let tilesFull = Vector.zip (Vector.fromList $ Map.keys packMap) tiles

      let
        tilesSparse = do
          (collection, inputs) <- tilesFull
          guard $ not (Map.null $ Tiles.groupTiles inputs)
          pure (collection, inputs)

      guard $ not (Vector.null tilesSparse)
      pure
        ( pack
        , tilesSparse
        )
  ByteString.writeFile
    (file <.> "v1" <.> "zst")
    (Zstd.compress Zstd.maxCLevel . BSL.toStrict $ CBOR.serialise v1)

load
  :: ( MonadReader env m
     , HasLogFunc env
     , MonadUnliftIO m
     )
  => FilePath
  -> Vector Tiles.ProcessGroup
  -> Vector FilePath
  -> Vector (Map FilePath Int)
  -> m ()
load file groups packs files = do
  logInfo $ "Loading scene from " <> fromString file
  -- v0_tiles <- liftIO $ readFileDeserialise file
  -- atomically do
  --   for_ (Vector.zip groups v0_tiles) \(group, loadedGroup) ->
  --     for_ (Vector.zip group loadedGroup) \(worker, newInput) ->
  --       Worker.pushInputSTM worker $ const newInput

  let packIds = Map.fromList . toList $ Vector.imap (flip (,)) packs
  PackageV1 v1_objects <- Zstd.fromFileWith
    (pure . CBOR.deserialise . BSL.fromStrict)
    (liftIO . CBOR.readFileDeserialise)
    (file <.> "v1" <.> "zst")

  actions <- atomically do
    -- XXX: wipe the scene first, as the V1 is sparse.
    for_ groups \group ->
      for_ group \worker ->
        Worker.pushInputSTM worker $ const (Tiles.Input mempty 0.5)

    actions <- newTVar mempty
    let
      register action =
        modifyTVar' actions (action :)

    for_ v1_objects \(packPath, packed) ->
      case Map.lookup packPath packIds of
        Nothing ->
          register . logWarn $ mconcat
            [ "Missing object pack: "
            , displayShow packPath
            ]
        Just packId ->
          case Vector.zip groups files !? packId of
            Nothing ->
              register . logWarn $ mconcat
                [ "Missing worker group for "
                , displayShow packPath
                , ": "
                , display packId
                ]
            Just (group, paths) ->
              for_ packed \(objectPath, tiles) ->
                case Map.lookup objectPath paths of
                  Nothing ->
                    register . logWarn $ mconcat
                      [ "Missing object "
                      , displayShow $ packPath </> objectPath
                      ]
                  Just objectId ->
                    case group !? objectId of
                      Nothing ->
                        register . logWarn $ mconcat
                          [ "Missing worker for object "
                          , displayShow $ packPath </> objectPath
                          , ": "
                          , display objectId
                          ]
                      Just process ->
                        Worker.pushInputSTM process $ const tiles
    pure actions

  readTVarIO actions >>= sequence_ . reverse
