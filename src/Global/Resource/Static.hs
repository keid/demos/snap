{-# LANGUAGE OverloadedLists #-}

module Global.Resource.Static
  ( UnlitColored
  , cubeWireframe
  ) where

import RIO.Local

import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Types (MonadVulkan, Queues)
import Geometry.Cube qualified as Cube
import Render.Unlit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import Resource.Region qualified as Region

type UnlitColored =
  ( UnlitColored.Model 'Buffer.Staged
  , Buffer.Allocated 'Buffer.Staged UnlitColored.InstanceAttrs
  )

cubeWireframe
  :: ( MonadVulkan env m
     , Resource.MonadResource m
     )
  => Queues Vk.CommandPool
  -> m ( Resource.ReleaseKey
       , UnlitColored
       )
cubeWireframe pools = Region.run do
  model <- Model.createStagedL
    (Just "cubeWireframe")
    pools
    Cube.bbWireColored
    Nothing
  Model.registerIndexed_ model

  attrs <- Buffer.createStaged
    (Just "cubeWireframe.instances")
    pools
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    [mempty]
  Buffer.register attrs

  pure (model, attrs)
