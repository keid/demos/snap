module Global.Resource.Object
  ( Files
  , Collection
  , Object(..)
  , loadModels
  , storeSettings
  , settingsFile
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (MonadResource, ResourceT)
import Codec.GlTF (GlTF)
import Codec.GlTF.Material qualified as GlTF (Material)
import Data.Tree (Tree)
import Geomancy.Vec3 qualified as Vec3
import RIO.Directory (doesFileExist, getDirectoryContents)
import RIO.FilePath (splitExtensions, (</>), (<.>))
import RIO.List qualified as List
import RIO.Map qualified as Map
import RIO.Vector qualified as Vector
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 (CommandPool)

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (MonadVulkan, Queues)
import Render.Lit.Colored.Model qualified as LitColored
-- import Render.Lit.Material.Model qualified as LitMaterial
import Resource.Buffer qualified as Buffer
import Resource.Collection qualified as Collection
import Resource.Gltf.Load qualified as Gltf
import Resource.Gltf.Model qualified as Gltf
import Resource.Gltf.Scene qualified as Gltf
import Resource.Gltf.Weld qualified as Weld
import Resource.Gltf.Weld.Settings (Settings)
import Resource.Gltf.Weld.Settings qualified as Settings
-- import Resource.Mesh.Lit qualified as MeshLit
import Resource.Mesh.Types qualified as Mesh
import Resource.Model qualified as Model
import Resource.Source qualified as Source

type Files = Map FilePath Int

type Collection = Vector Object

data Object = Object
  { source   :: FilePath
  , settings :: Settings

  , gltf     :: GlTF
  , scene    :: Tree Gltf.SceneNode

  , meta     :: Mesh.Meta
  , nodes    :: Mesh.Nodes
  , model    :: LitColored.Model 'Buffer.Staged

  -- , metaMat  :: Mesh.Meta
  -- , nodesMat :: MeshLit.MaterialNodes
  -- , modelMat :: LitMaterial.Model 'Buffer.Staged
  }
  deriving (Show, Generic)

loadModels
  :: Queues CommandPool
  -> FilePath
  -> ResourceT (StageRIO st) (Files, Collection)
loadModels pools modelsDir = do
  contents <- fmap List.sort $ getDirectoryContents modelsDir

  found <- for contents \path -> do
    let fullPath = modelsDir </> path
    isFile <- doesFileExist fullPath
    if isFile then
      case splitExtensions path of
        (name, ext)
          | ext `elem` [".glb", ".glb.zst", ".gltf", ".gltf.zst"] ->
              pure $ Just (name, fullPath)
        _ ->
          pure Nothing
    else
      pure Nothing

  let collected = Collection.enumerate $ catMaybes found

  let
    filesIds = Map.fromList do
      (ix, (name, _path)) <- collected
      pure (name, ix)

  objects <- for collected \(_ix, (name, path)) -> do
    let settingsPath = path <.> ".yaml"
    settings <- fmap (fromMaybe Settings.gltfMidBottom) $
      loadSettings settingsPath

    -- when (null $ Settings.transformStack settings) do
    --   Settings.storeFile
    --     ( settings
    --         { Settings.transformStack =
    --             Settings.transformStack Settings.gltfMidBottom
    --         }
    --     )
    --     settingsPath

    -- let p4m = path -<.> ".p4m"
    -- _processed <- doesFileExist p4m
    ---------------

    let optionsMaterialOffset = 0 -- TODO: remap materials
    (gltf, meshPrimitives) <- lift $
      Gltf.loadMeshPrimitives True True path
    sceneTreeOriginal <- lift $
      Gltf.unfoldSceneM
        optionsMaterialOffset
        (mconcat . mapMaybe Settings.parseTransform $ Settings.transformStack settings)
        gltf
        meshPrimitives

    let
      originalMeasurements = Weld.measureTree sceneTreeOriginal

      origins =
        [ Settings.originX settings
        , Settings.originY settings
        , Settings.originZ settings
        ]
      originV = Weld.originV originalMeasurements origins + Settings.adjustOrigin settings
      sceneTreeFixed = fmap (Weld.adjustPositions originV) sceneTreeOriginal

      fixedMeasurements = Weld.measureTree sceneTreeFixed

    (meta, nodes, mesh) <- loadMesh
      (fromString name)
      pools
      sceneTreeFixed
      fixedMeasurements
      (Weld.processMeshNode Weld.modelColoredLit Weld.ignoreMaterial)

    -- (metaMat, nodesMat, meshMat) <- loadMesh
    --   pools
    --   sceneTreeFixed
    --   fixedMeasurements
    --   (Weld.processMeshNode Weld.modelMaterial Weld.materialNode)
      -- Model.destroyIndexed meshMat

    pure $
      Object
        path
        settings
        gltf
        sceneTreeFixed
        meta
        nodes
        mesh
        -- metaMat
        -- nodesMat
        -- meshMat

  pure
    ( filesIds
    , Vector.fromList objects
    )

loadMesh
  :: ( Foldable t
     , Mesh.HasRange node
     , MonadVulkan env m
     , MonadResource m
     , Storable node
     , Storable attrs
     )
  => Text
  -> Queues CommandPool
  -> t Gltf.SceneNode
  -> Mesh.AxisAligned Mesh.Measurements
  -> (Gltf.SceneNode
      -> Maybe (Int, GlTF.Material)
      -> Gltf.Stuff
      -> (Mesh.NodeGroup, (Gltf.StuffLike attrs, node)))
  -> m
      ( Mesh.Meta
      , Storable.Vector node
      , Model.Indexed 'Buffer.Staged Vec3.Packed attrs
      )
loadMesh name pools sceneTreeFixed fixedMeasurements processNode = do
  let
    byBlendMode = Weld.partitionNodes $
      Weld.processSceneNodes
        processNode
        sceneTreeFixed

  -- traverse_ (traceShowM . fst) meshPrimitives
  -- error "halt"

  let
    nodePartitions = fmap (map snd . toList) byBlendMode
    nodesBadRanges = fold nodePartitions
    nodes = Storable.fromList $
      zipWith Mesh.adjustRange
        nodesBadRanges
        (List.scanl' (+) 0 $ map (Model.irIndexCount . Mesh.getRange) nodesBadRanges)

  let
    stuffByBlendMode = fmap (Gltf.mergeStuffLike . fmap fst) byBlendMode
    (positions, indices, attrs) = Gltf.mergeStuffLike stuffByBlendMode

  ---------------

  let
    countIndices (_pos, npIndices, _attrs) =
      fromIntegral $ Vector.length npIndices

    meta =
      Weld.sceneMeta
        fixedMeasurements
        (fmap countIndices stuffByBlendMode)
        nodePartitions

  mesh <- Model.createStaged
    (Just $ "Model:" <> name)
    pools
    (Storable.convert positions)
    (Storable.convert attrs)
    (Storable.convert indices)
  Model.registerIndexed_ mesh

  pure (meta, nodes, mesh)

loadSettings
  :: ( MonadReader env m
     , HasLogFunc env
     , MonadThrow m
     , MonadIO m
     )
  => FilePath
  -> m (Maybe Settings)
loadSettings p4s = do
  hasSettings <- doesFileExist p4s
  if hasSettings then
    fmap Just $
      Settings.load (Source.File Nothing p4s)
    -- liftIO (Yaml.decodeFileWithWarnings p4s) >>= \case
    --   Left err -> do
    --     logError $ mconcat
    --       [ "YAML error in settings file ", displayShow p4s, ":\n"
    --       , fromString $ Yaml.prettyPrintParseException err
    --       ]
    --     pure Nothing
    --   Right (warns, res) -> do
    --     traverse_ (logWarn . displayShow) warns
    --     pure res
  else
    pure Nothing

storeSettings :: HasLogFunc env => FilePath -> Settings -> RIO env ()
storeSettings p4s settings = do
  logDebug $ "Storing settings at " <> fromString p4s
  liftIO $ Settings.storeFile settings p4s

settingsFile :: Object -> FilePath
settingsFile Object{source} = source <.> ".yaml"
