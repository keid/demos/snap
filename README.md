# Snap

3D tile editor experiment using [basic] rendering and [gtlf] assets.

![2021-06-20](https://i.imgur.com/8w9XoGP.png)

[basic]: https://gitlab.com/keid/engine/-/tree/main/render-basic
[gltf]: https://gitlab.com/keid/engine/-/tree/main/resource-gltf

## Controls

### Mouse

* `Left button` - put selected tile at cursor/level, click again to rotate.
* `Shift + Left button` - remove selected tile at cursor/level.
* `Middle button` - drag to pan.
* `Scroll` - zoom in/out.
* «Side» buttons - change levels up/down.

### Keyboard

* `Arrows` - hold to pan.
* `PageUp`/`PageDown` - change levels up/down.
* `Insert`/`Delete` - turn camera around.
* `Home`/`End` - raise/lower inclination.

## Scene format

Zstd-compressed CBOR-encoded scene dump, for now.

## Assets format

GLB (glTF «binary» packages) with YAML metadata.
